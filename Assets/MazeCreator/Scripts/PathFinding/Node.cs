﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace MazeCreator
{
/*  Used as a node in the pathfinding algoritm */
public class Node : Cell, IComparable {
	public readonly int gGcost = 1;
	public int hCost;
	public readonly Vector3 position;
	public Node parent;

	#region Properties
	public int Fcost 
	{
		get 
		{
			return gGcost + hCost; 
		}
	}

	#endregion

#region Constructor
	public Node(CellIndex index, Vector3 nodePostionInGrid,Wall[] walls): base (index.i,index.j)
	{
		this.CellIndexInGrid = index;
		this.position = nodePostionInGrid;
		this.Walls = new List<Wall>(walls);
	}	
#endregion	

#region IComparer
	public int CompareTo(object obj)
	{	
		Node other = (Node)obj;
		int cost = Fcost.CompareTo(other.Fcost);		
		if (cost == 0)
		{
			cost = gGcost.CompareTo(other.gGcost);
		}
		return cost;
	}
#endregion
}
}