﻿using System.Collections;
using System.Collections.Generic;
using System;

namespace MazeCreator
{
public class Heap<T> where T : IComparable  
{
#region Fields
	private List<T> _items;
	private const int HEAP_HEAD = 0;
#endregion

#region Properties
 	public int Count 
	{
		get 
		{
			return _items.Count;
		}
	}
#endregion


#region Constructors
	public Heap()
	{
		_items = new List<T>();
	}

	public Heap(int heapSize)
	{
		_items = new List<T>(heapSize);
	}
#endregion

#region Modifing List
	public void Add(T item)
	{
		_items.Add(item);
		SortUp();
	}

	public T GetMinItem()
	{
		T item = _items[HEAP_HEAD];
		Swap(0,_items.Count -1); //Move the last element of the heap to the head		
		SortDown();
		_items.Remove(item); //Remove the element from the heap
		return item;
	}
#endregion


#region Sorting
	public void SortUp()
	{
		int itemIndex = _items.Count - 1;
		bool sorted = false;
		 while (sorted == false)
		 {
			 int parentIndex = (itemIndex - 1) / 2;
			 //Move the smalles element at the top
			 if (_items[itemIndex].CompareTo(_items[parentIndex]) > 0)
			 {
				 Swap(itemIndex,parentIndex);
				 itemIndex = parentIndex;
			 }
			 else
			 {
				 sorted = true;
			 }			 
		 }
	}

	/* Start from the heap head, find the two child, if one of two child is smallest than the parent,
	 swap them, otherwise exit
	*/
	public void SortDown()
	{
		int currentIndex = HEAP_HEAD;
		int swapIdex = HEAP_HEAD;
		int lastItemIndex = _items.Count - 1 ;
		bool sorted = false;
		do
		{
			currentIndex = swapIdex;
			int leftChildIndex = ((currentIndex * 2) + 1);
			int rightChildIndex = ((currentIndex * 2) + 2);
			if (leftChildIndex < lastItemIndex ) //Checking if left item exsist
			{
				swapIdex = leftChildIndex;
				//Checking if right item exsist
				//Checking if right is smaller than left, and if it is, right will be element to became the father
				if (rightChildIndex < lastItemIndex && 
					_items[rightChildIndex].CompareTo(_items[leftChildIndex]) < 0) 
				{				
					swapIdex = rightChildIndex;
				}
				//If the new element is smaller than the father, swap them
				if (_items.Count > 0 && _items[swapIdex].CompareTo(_items[currentIndex]) < 0)
				{
					Swap(swapIdex,currentIndex);				
				}			
				else
				{
					sorted = true; //No more smaller element to swap.
				}
			}
			else
			{
				sorted = true; //No child, arrived at the bottom
			}						
		} while (sorted == false);

	}

	private void Swap(int firstIndex, int secondIndex)
	{
		T tmpElemnt = _items[firstIndex];
		_items[firstIndex] = _items[secondIndex];
		_items[secondIndex] = tmpElemnt;
	}
#endregion	

#region Helpers
	public bool Contains(T item)
	{
		return _items.Contains(item);
	}

	public List<T> GetElements()
	{
		return _items;
	}
#endregion
}
}