﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace MazeCreator
{
public static class AStar {
	
	#region Fields
	 private static PathFindingGridManager _gridManager;
	#endregion
		

#region PathFinding
	public static List<Node> FindPath(Vector3 startPosition, Vector3 endPosition)
	{
		SetGrid();
		//Get the start Node and end Node from the grid
		Node startNode = _gridManager.WordToGridPosition(startPosition);
		Node targetNode = _gridManager.WordToGridPosition(endPosition);
		//Initialize the open and close set
		Heap<Node> openSet = new Heap<Node>();
		openSet.Add(startNode);
		List<Node> closeSet = new List<Node>();

		while (openSet.Count > 0)
		{
			Node currentNode = openSet.GetMinItem();

			if (currentNode == targetNode) 
			{
				return ReversePath(targetNode,startNode);
			}

			foreach (Node neghboard in _gridManager.GetReachableNeghboards(currentNode))
			{
				if (closeSet.Contains(neghboard) == false)
				{
					neghboard.hCost = GetDistanceBetweenNode(neghboard,targetNode);
					if (openSet.Contains(neghboard) == false || neghboard.Fcost < currentNode.Fcost)
					{						
						closeSet.Add(currentNode);		
						if (openSet.Contains(neghboard) == false)
						{
							neghboard.parent = currentNode;	
							openSet.Add(neghboard);
						}
					}
				}
			}			
		}
		Debug.LogWarning("No path has be found for the node "+ startNode.CellIndexInGrid.i + startNode.CellIndexInGrid.j + 
		" and the node " + targetNode.CellIndexInGrid.i + targetNode.CellIndexInGrid.j);
		return null;
	}
#endregion

	private static int GetDistanceBetweenNode(Node startNode, Node endNode)
	{
		return Mathf.Abs(startNode.CellIndexInGrid.i - endNode.CellIndexInGrid.i) + Mathf.Abs(startNode.CellIndexInGrid.j - endNode.CellIndexInGrid.j);
	}

	private static void SetGrid()
	{
		if (_gridManager == null)
		{
			_gridManager = GameObject.FindObjectOfType<PathFindingGridManager>();
		}
		_gridManager.ResetNeghboards();

	}

	private static List<Node> ReversePath(Node endNode, Node startNode)
	{
		List <Node> path = new List<Node>();
		Node current = endNode;
		while (current.parent != null)
		{
			path.Add(current);
			current = current.parent;
		}
		path.Add(startNode);
		return path;
	}
}
}