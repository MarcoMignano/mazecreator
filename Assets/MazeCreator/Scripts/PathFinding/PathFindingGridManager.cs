﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MazeCreator
{
[ExecuteInEditMode]
public class PathFindingGridManager : MonoBehaviour {

#region Fields
	private Node [,] _grid;	
	private float _mazeLenght;
#endregion

#region MonoBehaviour
	// Use this for initialization
	void OnEnable () 
	{				
		BuilGrid();		
	}
#endregion

#region Initilization
	public void BuilGrid()
	{
		List<GameObject> cells = transform.GetChilds();					
		InitGridSize();
		foreach (GameObject cell in cells)
		{
			CellIndex cellIndex = MazeUtilities.GetCellIndexFromName(cell.name);
			Wall[] walls = MazeUtilities.GetWallsFromCell(cell);
			
			_grid[cellIndex.i,cellIndex.j] = new Node(cellIndex,cell.transform.position,walls);
		}
		_mazeLenght =  _grid.GetLength(0) * cells[0].transform.localScale.x;
	}

	private void InitGridSize()
	{
		List<GameObject> cells = transform.GetChilds();			
		int gridSize = (int) Math.Sqrt((double) cells.Count);
		_grid = new Node[gridSize,gridSize];
	}
#endregion

#region Utilities
	public Node WordToGridPosition(Vector3 worldPosition)
	{
		/* Find the distance on the GRID in each axsist and express it between 0 and 1 where 0 is the far left on 0 is the far right of the grid.
		   For example if e click at -2 and the grid has size 10 and centerd in the 0, 
		   the percentX will be - 2 + 5 / 10 = 0.3, wich means that if we take the grid widh, we clicked at 30% of that Widh
		*/

		/*
			Adjusting the value of the Y position based on where the camera Y is, usually the camera y is set to one so we 
			need to simulate that the camera position is centerd on the screen and we do this by add the offset of the camere to the 
			poosition that we're looking for
		*/
		float wordPosY = worldPosition.y - SceneUtilities.Camera.transform.position.y; 
		float percentX = (_mazeLenght/2 + worldPosition.x) /_mazeLenght;
		float percentY = (_mazeLenght/2 - wordPosY) /_mazeLenght;
		
	
		//Ger the index from the percented distance alng each axsis
		int x = Mathf.RoundToInt((float) Math.Floor(_grid.GetLength(0) * percentX)) ;
		int y = Mathf.RoundToInt((float) Math.Floor(_grid.GetLength(1) * percentY)) ;

		//Clamp the resoult between 0 and the lenght of the grid to be soure that is inside the grid
		x = Mathf.Clamp(x,0,_grid.GetLength(0) -1);
		y = Mathf.Clamp(y,0,_grid.GetLength(1) -1);
				
		
		return _grid[y,x];
	}

	public bool CanGoToDirection(Vector3 fromPosition,Vector3 direction)
	{
		Directions dir = FromVectorTiDirection(direction);
		if (dir == Directions.Undefined) 
		{
			return false; 
		}
		Node startNode = WordToGridPosition(fromPosition);		
		return !startNode.HasWallInDirection(dir);
	}

	private bool IsInsideTheGrid(CellIndex index)
	{
		return (index.i >= 0 && index.i < _grid.GetLength(0)) && (index.j >= 0 && index.j < _grid.GetLength(1));
	}

	private Directions FromVectorTiDirection(Vector3 vectroDir)
	{
		if (vectroDir == Vector3.up) {return Directions.North; }
		else if (vectroDir == Vector3.right) {return Directions.East; }
		else if (vectroDir == Vector3.down) {return Directions.South; }
		else if (vectroDir == Vector3.left) {return Directions.West; }
		else { return Directions.Undefined; }
	}
	

	
#endregion

//Section used to get Neghboards and othe infos
#region Neghboards
	
	public Vector3 GetNeghboardPositionInDirection(Vector3 fromPosition,Vector3 direction)
	{
		Vector3 destPosition = Vector3.zero;
		Directions dir = FromVectorTiDirection(direction);
		if (dir != Directions.Undefined) 
		{
			Node startNode = WordToGridPosition(fromPosition);		
			if (startNode.HasWallInDirection(dir) == false)
			{
				CellIndex neghboardIndex = MazeUtilities.GetNeighbordIndexFromDirection(startNode.CellIndexInGrid,dir);
				if (IsInsideTheGrid (neghboardIndex))
				{
					destPosition = _grid[neghboardIndex.i,neghboardIndex.j].position;
				}
			}	
		}
		return destPosition;		
	}
	//Used to get the neghboards sourrinding a cell
	public List<Node> GetReachableNeghboards(Node node)
	{
		List<Node> neghboards = new List<Node>();
		foreach (Directions direction in Enum.GetValues(typeof(Directions)))
		{
			if (!node.HasWallInDirection(direction))
			{
				CellIndex neghboardIndex = MazeUtilities.GetNeighbordIndexFromDirection(node.CellIndexInGrid,direction);
				if (IsInsideTheGrid (neghboardIndex))
				{
					neghboards.Add(_grid[neghboardIndex.i,neghboardIndex.j]);
				}
			}
		}
		return neghboards;
	}

	public void ResetNeghboards()
	{
		for (int i = 0; i < _grid.GetLength(0); i++)
		{
			for (int j = 0; j < _grid.GetLength(1); j++)
			{
				_grid[i,j].parent = null;
			}
		}
	}

	
#endregion
}
}