﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MazeCreator;
using UnityEngine.Assertions;

public class PlayerGMV2 : MonoBehaviour {

	public float speed = 1;
	private Vector3 _dir = Vector3.zero;
	private Vector3 _nextRequestedDir = Vector3.zero;
	private Vector3 _positionToReach = Vector3.zero;
	private PathFindingGridManager _gridManager;
	[HideInInspector]
	public List<string> doorsKey;
	
	void Awake()
	{
		_gridManager = GameObject.FindObjectOfType<PathFindingGridManager>();
	}
	// Use this for initialization
	void Start () {
		doorsKey = new List<string>();
		//float colliderSize = GameObject.Find("Maze").transform.GetChilds()[0].transform.localScale.x / 2;
		//colliderSize -= (GameObject.Find("Maze").transform.GetChilds()[0].transform.GetChilds()[0].transform.localScale.y * 2) - 0.002f;
		//GetComponent<BoxCollider2D>().edgeRadius = colliderSize;
		//Debug.Log("coll " + colliderSize);	

		_positionToReach = transform.position;
	}
	
	// Update is called once per frame
	void Update () {		
		if ( Input.GetKey(KeyCode.UpArrow) ) {DirectionReceived(Vector3.up); }

	    if ( Input.GetKey(KeyCode.DownArrow) ) {DirectionReceived(Vector3.down) ;}
    
  		if ( Input.GetKey(KeyCode.RightArrow) ) {DirectionReceived(Vector3.right); }
    
  		if ( Input.GetKey(KeyCode.LeftArrow) ) {DirectionReceived(Vector3.left); }

		// if (dir != Vector3.zero)
		// 	transform.position = transform.position + dir  * speed * Time.deltaTime;
		// }

		Move();

	}

	private void Move()
	{
//		if (OvershootTarget())
//		{
//			
//		}
		if (transform.position == _positionToReach)
		{
			//transform.localPosition = _positionToReach;

			if (_gridManager.CanGoToDirection(transform.position, _nextRequestedDir))
			{
				DirectionReceived(_nextRequestedDir);
			}
			else if (_gridManager.CanGoToDirection(transform.position, _dir))
			{
				DirectionReceived (_dir);
			}
			else
			{
				Stop();
			}

		}
		else
		{
			float step = speed * Time.deltaTime;
			transform.position = Vector3.MoveTowards(transform.position, _positionToReach, step);			
		}
	}

	private void DirectionReceived(Vector3 direction)
	{
		if (transform.position != _positionToReach)
		{
			if (_gridManager.CanGoToDirection(transform.position,direction))
			{
				_nextRequestedDir = direction;
			}
		}
		else
		{
			if (_gridManager.CanGoToDirection(transform.position,direction))
			{
				SetNewDir(direction, _gridManager.GetNeghboardPositionInDirection(transform.position,direction));
			}
			else if (_gridManager.CanGoToDirection(transform.position,direction) && IsOppositeDirection(direction))
			{
				SetNewDir(direction, _gridManager.GetNeghboardPositionInDirection(transform.position,direction));
			}
			else if (_dir != Vector3.zero)
			{
				_nextRequestedDir = direction;
			}
		}
	}

	private bool IsOppositeDirection(Vector3 newDirection)
	{
		return (Vector3.Dot(_dir,newDirection) < 0);
	}

	private void SetNewDir(Vector3 direction, Vector3 nextWaypoint)
	{
		this._dir = direction;

		this._positionToReach =  nextWaypoint;
		Assert.IsFalse((this._positionToReach == Vector3.zero),"ERROR: The position you're traing to reach in null");
		_nextRequestedDir = Vector3.zero;
	}

	private void Stop()
	{
		_positionToReach = transform.position;
		_dir = _nextRequestedDir = Vector3.zero;		  
	} 		
}
