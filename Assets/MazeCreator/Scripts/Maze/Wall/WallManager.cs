﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MazeCreator
{
[ExecuteInEditMode]
//Used to ecapsulate the wall in a GameObject used in the scene
public class WallManager : MazeMonobeheavior {
	public Wall wall {get; private set;} 

	// void Awake()
	// {
	// 	if (gameObject.GetComponent<BoxCollider2D>() == null)
	// 	{
	// 		gameObject.AddComponent<BoxCollider2D>();		
	// 	}
		
	// }
	public void InitWall(Directions direction)
	{
		this.wall = new Wall(direction);
	}
	
}
}