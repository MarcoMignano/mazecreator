﻿using System;
using UnityEngine.Assertions;

namespace MazeCreator
{
public enum Directions
{
	North,
	East,
	South,
	West,
	Undefined

}
public enum OppositeDirections
{
	North = Directions.South,
	East = Directions.West,
	South = Directions.North,
	West = Directions.East,
}

public struct Wall
{

	
	#region Fields
	//Used to identify where is the wall inside the singe cell
	public readonly Directions wallDirection;
	public readonly Directions oppositeWallDirection;
	#endregion
	
	#region Constructor
	public Wall(Directions direction)
	{
		this.wallDirection = direction;
		switch (direction)
		{
			case Directions.North:
			{
				this.oppositeWallDirection = (Directions) OppositeDirections.North;
				break;
			}			
			case Directions.East:
			{
				this.oppositeWallDirection = (Directions) OppositeDirections.East;
				break;
			}
			case Directions.South:
			{
				this.oppositeWallDirection = (Directions) OppositeDirections.South;
				break;
			}
			case Directions.West:
			{
				this.oppositeWallDirection = (Directions) OppositeDirections.West;
				break;
			}	
			default:
			{
				this.oppositeWallDirection = (Directions) OppositeDirections.North;				
				break;		
			}			
		}		
	}
	#endregion
}
}