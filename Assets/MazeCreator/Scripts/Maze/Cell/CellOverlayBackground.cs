﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Assertions;
using UnityEngine;
using UnityEditor;

namespace MazeCreator
{
    [ExecuteInEditMode]
    //Used at run/editor time edit and manpulate the cell
    public class CellOverlayBackground : MazeMonobeheavior
    {
        private Color _cellAlphaColor;
        private Color _cellSelectedAlphaColor;
        private SpriteRenderer _spriteRend;
        // Use this for initialization

        /// <summary>
        /// Reset is called when the user hits the Reset button in the Inspector's
        /// context menu or when adding the component the first time.
        /// </summary>
        void Reset()
        {
            InitSpriteRenderer();
            InitSpireColor();
            InitSpriteSize();
            AddBoxCollider();
        }

        private void Start()
        {
            InitSpriteRenderer();
        }
        public void OnMouseEnter()
        {
            _spriteRend.color = _cellSelectedAlphaColor;
        }

        public void OnMouseExit()
        {
            _spriteRend.color = _cellAlphaColor;
        }

        internal override void CleanCell()
        {
            Component[] components = GetComponents(typeof(Component));
            foreach (Component component in components)
            {
                if (component.GetType() != typeof(Transform))
                {
                    DestroyImmediate(component);
                }
            }
        }
        private void InitSpriteRenderer()
        {
            Sprite cellBackground = Resources.Load(Constants.Resurces.CELL_BACKGROUND, typeof(Sprite)) as Sprite;
            Assert.IsTrue(cellBackground != null, "ERROR: the cellBackground " + Constants.Resurces.CELL_BACKGROUND + " has not been found");
            if (gameObject.GetComponent<SpriteRenderer>() == null)
            {
                gameObject.AddComponent<SpriteRenderer>();    
            }
            _spriteRend = gameObject.GetComponent<SpriteRenderer>();    

            if (_spriteRend.sprite == null) _spriteRend.sprite = cellBackground;
        }

        private void InitSpireColor()
        {
            _cellAlphaColor = gameObject.GetComponent<SpriteRenderer>().color;
            _cellAlphaColor.a = 0f;
            _spriteRend.color = _cellAlphaColor;
            _cellSelectedAlphaColor = _cellAlphaColor;
            _cellSelectedAlphaColor.a = 0.5f;
        }

        private void InitSpriteSize()
        {
            _spriteRend.drawMode = SpriteDrawMode.Sliced;
            _spriteRend.size = new Vector2(1, 1);
        }

        private void AddBoxCollider()
        {
            gameObject.AddComponent<BoxCollider2D>().size = new Vector2(1, 1);
        }
    }
}