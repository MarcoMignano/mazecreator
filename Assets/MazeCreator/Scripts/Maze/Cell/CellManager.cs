﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace MazeCreator
{
    public class CellManager : MazeMonobeheavior
    {

        private MazeCell _cell;
        private GameObject _wallPrefab;

        #region Properties
        public CellIndex Index
        {
            get
            {
                return _cell.CellIndexInGrid;
            }
        }
        #endregion
        #region Initialization
        public void InitCellManager(CellIndex index, GameObject wallPrefab)
        {
            this._cell = new MazeCell(index.i, index.j);
            this._wallPrefab = wallPrefab;
        }
        #endregion

        #region Wall building
        public void CreateWall(Directions direction)
        {
            if (!_cell.HasWallInDirection(direction) && direction != Directions.Undefined)
            {
                //Add wall to the scene
                GameObject wallGameObj = BuildWall(direction);
                wallGameObj.name = direction.ToString() + "_wall";
                //Add the wall to the internal data structure
                _cell.AddWall(wallGameObj.GetComponent<WallManager>());
                CreteWallInOppositeCell(direction);
            }
        }

        private void CreteWallInOppositeCell(Directions wallDirection)
        {
            GameObject neighbord = GetNeighbordInDirection(wallDirection);
            if (neighbord != null)
            {
                neighbord.GetComponent<CellManager>().CreateWall(_cell.GetOppositeWallInDirection(wallDirection));
            }
        }

        //Used by the LoadManger to add exsisting wall to the cell manager internal data structure
        public void AddExsistingWall(Directions direction, GameObject wallGameObj)
        {
            if (!_cell.HasWallInDirection(direction))
            {
                //Add the wall to the internal data structure
                _cell.AddWall(wallGameObj.GetComponent<WallManager>());
            }
        }
        #endregion

        #region Remove wall
        public bool RemoveWall(Directions direction)
        {
            if (_cell.HasWallInDirection(direction))
            {
                GameObject wallToRemove = _cell.GetWallInDirection(direction);
                _cell.RemoveWall(wallToRemove.GetComponent<WallManager>());
                GameObject.DestroyImmediate(wallToRemove);
                RemoveWallInOppositeCell(direction);
                return true;
            }
            else
            {
                return false;
            }
        }

        private void RemoveWallInOppositeCell(Directions wallDirection)
        {
            GameObject neighbord = GetNeighbordInDirection(wallDirection);
            if (neighbord != null)
            {
                neighbord.GetComponent<CellManager>().RemoveWall(_cell.GetOppositeWallInDirection(wallDirection));
            }
        }
        #endregion

        public GameObject GetNeighbordInDirection(Directions direction)
        {
            CellIndex neighbordIndex = MazeUtilities.GetNeighbordIndexFromDirection(_cell.CellIndexInGrid, direction);
            if (MazeUtilities.IsInsideMaze(neighbordIndex))
            {
                return GameObject.Find(MazeUtilities.GetCellNameFromIndex(neighbordIndex));
            }
            return null;
        }
        private GameObject BuildWall(Directions direction)
        {
            Vector2 wallPosition = MazeBuilder.GetWallPosition(direction, transform.localScale.x, transform.position);
            Wall wall = new Wall(direction);
            return WallBuilder.BuildWall(wall, wallPosition, _wallPrefab, transform.localScale.x, transform);
        }
    }
}