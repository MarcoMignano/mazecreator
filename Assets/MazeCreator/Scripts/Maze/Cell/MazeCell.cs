﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace MazeCreator
{
//Used to rappresent the Cell and the walls used in the game. Used by the MazeManager to add/remove wall during level design
public class MazeCell : Cell {

	private Dictionary <Directions, GameObject> _wallsGameObj = new Dictionary<Directions, GameObject>();

	public MazeCell (int i, int j) : base (i,j)
	{
		this.Walls = new List<Wall> ();
	}

	public void AddWall(WallManager mazeWall)
	{
		Walls.Add(mazeWall.wall);
		_wallsGameObj.Add(mazeWall.wall.wallDirection,mazeWall.transform.gameObject);
	}

	public void RemoveWall(WallManager mazeWall)
	{
		Walls.Remove(mazeWall.wall);
		_wallsGameObj.Remove(mazeWall.wall.wallDirection);
	}

	public override bool HasWallInDirection(Directions direction)
	{
		return _wallsGameObj.ContainsKey(direction);
	}

	public GameObject GetWallInDirection(Directions direction)
	{
		Assert.IsTrue(HasWallInDirection(direction:direction),"ERROR: the cell has no value in the selected Direction");
		return _wallsGameObj[direction];
	}

	public Directions GetOppositeWallInDirection(Directions direction)
	{
		foreach (Wall wall in Walls)
		{
			if (wall.wallDirection == direction)
			{
				return wall.oppositeWallDirection;
			}
		}
		return Directions.Undefined;
	}
}
}