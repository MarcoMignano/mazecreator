﻿using System.Collections.Generic;
using UnityEngine;

namespace MazeCreator
{
//Used to describe the basic instance that rappresents a cell, used to build the maze by the DFS algorithm.
public class Cell 
{
	#region Poperties
	public bool IsVisited { get; set; }
	public CellIndex CellIndexInGrid { get; internal set; }
	public List<Wall> Walls { get; internal set; }
	
	#endregion

	public Cell(int i, int j)
	{
		this.CellIndexInGrid = new CellIndex (i,j);		
		this.Walls = new List<Wall>  { new Wall(Directions.North),
									   new Wall(Directions.East),
									   new Wall(Directions.South),
									   new Wall(Directions.West) };
		this.IsVisited = false;
	}

	public void RemoveWallInDirection(Directions direction)
	{
		foreach (Wall wall in Walls)
		{
			if (wall.wallDirection == direction)
			{
				Walls.Remove(wall);
				break;
			}
		}
	}

	public virtual bool HasWallInDirection(Directions direction)
	{
		//if the direction is undefined, return true so to be sure to not to go or move in that direction
		if (direction == Directions.Undefined)
		{
			return true;
		}
		foreach (Wall wall in Walls)
		{
			if (wall.wallDirection == direction)
			{
				return true;
			}
		}
		return false;
	}
}

public struct CellIndex
	{
		public readonly int i;
		public readonly int j;	
		
		public CellIndex(int i, int j)
		{
			this.i = i;
			this.j = j;
		}
	}
}