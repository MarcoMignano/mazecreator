﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MazeCreator
{
public class MazeInfo
{
	#region Fields
	private static MazeInfo _instance;
	#endregion
	
	#region Poperties
	public Vector2 MazeOrigin {get; private set;}
	public int MazeSizeX {get; private set;}
	public int MazeSizeY {get; private set;}
	public float SquareSize {get; private set;}
	public float MazeMargin {get; private set;}
	public float WallThickness {get; private set;}
	#endregion

	#region Singelton
	public static MazeInfo Instance
	{
		get 
		{
			if (_instance == null)
			{
				_instance = new MazeInfo();
			}
			return _instance;
		}		
	}
	#endregion

/// <summary>
/// This function is used to initialize the maze info.
/// mazeRaws = the size of the maze
/// mazeMargin = the margin that the maze has to have from the left border and the right border, example 0.1 = 10% of 
///margin on the left and right side of the screen
/// </summary>
	public void InitMazeInfo(int mazeRaws, float mazeMargin, float wallSize)
	{
		SetMazeInfo(mazeRaws,mazeMargin,wallSize);
	}

	private void SetMazeInfo(int mazeRaws, float mazeMargin, float wallSize)
	{
		//Get the bottom Left of the creen
		float screenOriginLeft = SceneUtilities.Camera.ViewportToWorldPoint(new Vector3(0, 0, SceneUtilities.Camera.nearClipPlane)).x;	
		//Get the bottom Right of the creen
		float screenOriginRight = SceneUtilities.Camera.ViewportToWorldPoint(new Vector3(1, 0, SceneUtilities.Camera.nearClipPlane)).x;
		//Get the center Right of the creen
		float centerOfTheScreen = SceneUtilities.Camera.ViewportToWorldPoint(new Vector3(0, 0.5f, SceneUtilities.Camera.nearClipPlane)).y;
		//Get the Width of the creen
		float screenWidth =  screenOriginRight - screenOriginLeft;
		//Get the Margin of the maze from teh border (left and right)
		float screenMargin = (screenWidth * mazeMargin);
		//Get the Sie of a signe square
		float squareSize = (screenWidth - screenMargin * 2) / mazeRaws;				
		//Get the height of the maze
		float mazeHeight = squareSize * mazeRaws;
		//Find the initiale position of the center of the first cell to be positioned
		float originTablePositionX = screenOriginLeft + screenMargin + squareSize/2;
		float originTablePositionY = centerOfTheScreen + mazeHeight / 2 - squareSize/2;

		this.MazeOrigin = new Vector2(originTablePositionX,originTablePositionY);
		this.SquareSize = squareSize;
		this.MazeMargin = mazeMargin;	
		this.WallThickness = wallSize;
		this.MazeSizeX = mazeRaws;
		this.MazeSizeY = mazeRaws;
	}
}
}