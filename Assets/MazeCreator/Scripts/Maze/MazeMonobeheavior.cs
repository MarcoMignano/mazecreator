﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MazeCreator
{
[ExecuteInEditMode]
public class MazeMonobeheavior : MonoBehaviour {

	void OnEnable()
	{
		EventManager.onCleanMazeDelegate += CleanCell;
	}
	void OnDisable()
	{
		EventManager.onCleanMazeDelegate -= CleanCell;
	}

	virtual internal void CleanCell()
	{
		DestroyImmediate(this);
	}
}
}