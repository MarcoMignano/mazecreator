﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;
using System.Text.RegularExpressions;

namespace MazeCreator
{
//Used to load a maze from the scene and initialize the internal data structures needed to edit the maze
public static class MazeLoader {

	private static readonly string CELL_ID = "cell";	
	public static void LoadMaze(GameObject wallPrefab)
	{
		GameObject maze = GameObject.Find(Constants.GameObjecName.MAZE);
		Assert.IsNotNull(maze,"ERROR: There is no maze to load");

		List<GameObject> cells = SceneUtilities.GetChilds(maze,CELL_ID);		
		int gridSize = (int) Math.Sqrt((double) cells.Count);
		float cellSize = cells[0].transform.localScale.x;
		float mazeMargin = GetMazeMargin (cellSize,gridSize);
		
		MazeInfo.Instance.InitMazeInfo(gridSize,mazeMargin,wallPrefab.transform.localScale.y);		
		foreach (GameObject cell in cells)
		{
			AssignWallInCell(cell,wallPrefab);
		}	
	}

	private static float GetMazeMargin(float cellSize, int gridSize)
	{
		return (gridSize * cellSize)/2;
	}
	private static void AssignWallInCell(GameObject cell, GameObject wallPrefab)
	{
		CellManager cellManager = cell.GetComponent<CellManager>();
		if (cellManager == null)
		{
			cellManager = CreateCellManager(cell,wallPrefab);
		}
		else
		{
			//cell manager already exsisting in the cell, so le maze was already loaded
			return;
		}
		AddWallToCell(cell);
	}

	private static void AddWallToCell(GameObject cellGameObject)
	{
		foreach (Transform child in cellGameObject.transform)
		{
			if (child.name.Contains(Constants.GameObjecName.WALL_ID))
			{
				Directions wallDirection = MazeUtilities.GetDirectionFromWallName(child.name);
				child.gameObject.AddComponent<WallManager>().InitWall(wallDirection);
				cellGameObject.GetComponent<CellManager>().AddExsistingWall(wallDirection,child.gameObject);
			}
		}
	}
	
	//Return the Direction of the wall based on the wall name
	
	
	//Create CellManager and assign it to the cell
	private static CellManager CreateCellManager(GameObject cellGameObject,GameObject wallPrefab)
	{
		CellIndex cellIndex = MazeUtilities.GetCellIndexFromName(cellGameObject.name);		
		cellGameObject.AddComponent<CellManager>().InitCellManager(cellIndex,wallPrefab);
		cellGameObject.AddComponent<CellOverlayBackground>();
		return cellGameObject.GetComponent<CellManager>();
	}	
}
}