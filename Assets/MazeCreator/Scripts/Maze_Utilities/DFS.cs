﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace MazeCreator
{
public static class DFS {

	private static Wall[] wallsToDestroy = new Wall[] { new Wall(Directions.North),
														   new Wall(Directions.East),
														   new Wall(Directions.South),
														   new Wall(Directions.West)};	
	
	public static Cell[,] CreateMaze(Cell[,] maze)
	{
		Cell firstCellToVisit = maze[UnityEngine.Random.Range(0,maze.GetLength(0)),
									UnityEngine.Random.Range(0,maze.GetLength(1))];
		firstCellToVisit.IsVisited = true;
		Stack<Cell> cellsToVisit = new Stack<Cell>();
		cellsToVisit.Push(firstCellToVisit);
		return DeepFirstSearch(ref maze, ref firstCellToVisit,ref cellsToVisit);	
	}

	private static Cell[,] DeepFirstSearch(ref Cell[,] maze, ref Cell currentCell, ref Stack<Cell> cellsToVisit)
	{
		DFS.wallsToDestroy.Shuffle();		
		foreach (Wall wallToDestroy in wallsToDestroy)
		{
			Cell nextCellToVisit = GetNextNeighbourdToVisit(wallToDestroy,ref maze, currentCell);
			if (nextCellToVisit != null)
			{
				nextCellToVisit.IsVisited = true;
				cellsToVisit.Push(nextCellToVisit);
				RemoveWall(ref currentCell, ref nextCellToVisit,wallToDestroy);
				DeepFirstSearch(ref maze,ref nextCellToVisit,ref cellsToVisit);
				break; //we found on passage to we can exit the For loop
			}						
		}
		if (cellsToVisit.Count > 0)
		{
			Cell newCellToVisit = cellsToVisit.Pop();
			DeepFirstSearch(ref maze,ref newCellToVisit,ref cellsToVisit);
		}
		return maze;
	}

	private static Cell GetNextNeighbourdToVisit(Wall wallToVisit, ref Cell[,] maze, Cell currentCell)	
	{		
		/*Get the cell in the direction of the wallToVisit, if the wall is a north wall for example then the
		 cell to visit will be the one obove the current one.
		*/
				CellIndex neighbourdIndex = MazeUtilities.GetNeighbordIndexFromDirection(currentCell.CellIndexInGrid,wallToVisit.wallDirection);
		int raw = neighbourdIndex.i;
		int coulum = neighbourdIndex.j;
		if (IsValidIndex(ref maze, raw, coulum))
		{
			Cell newCellToVisit = maze[raw,coulum];

			if ( newCellToVisit != null && newCellToVisit.IsVisited == false)
			{
				return newCellToVisit;
			}			
		}
		return null;
		
	}	

	private static void RemoveWall(ref Cell currentCell, ref Cell nextCell, Wall wallToRemove)
	{
		currentCell.RemoveWallInDirection(wallToRemove.wallDirection);
		/*In the next the cell to visit, the position ot the wall to remove will be the opposite of the current wall
		to remove since they are mirrored
		*/
		nextCell.RemoveWallInDirection(wallToRemove.oppositeWallDirection);
	}

	private static bool IsValidIndex(ref Cell[,] maze, int i , int j )
	{
		return ((i < maze.GetLength(0) && i >= 0) && 
			    (j < maze.GetLength(1) && j >= 0));
	}

}
}