﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

namespace MazeCreator
{
    //Class used to build wall into the scene
    public static class WallBuilder
    {
        public static void CreateWall(CellManager cell, Directions direction)
        {
            cell.CreateWall(direction);
        }

        public static void RemoveWall(CellManager cell, Directions direction)
        {
            cell.RemoveWall(direction);
            Wall neighbourdWall = new Wall(direction);
            //find the neighbourd that confine to the wall
            GameObject neighbourdCell = cell.GetNeighbordInDirection(direction);
            if (neighbourdCell != null)
            {
                CellManager cellManager = neighbourdCell.GetComponent<CellManager>();
                //Remove the wall from the negbourd
                cellManager.RemoveWall(neighbourdWall.oppositeWallDirection);
            }
        }

        //Method used to Buil a wall and add position it into the scene
        public static GameObject BuildWall(Wall wall, Vector2 wallPosition, GameObject wallGameObj, float squareSize, Transform parent)
        {
            //Crete the wall to be position on the game world
            GameObject wallToPostionGameObject = GameObject.Instantiate(wallGameObj, wallPosition, Quaternion.identity);
            wallToPostionGameObject.AddComponent<WallManager>().InitWall(wall.wallDirection);
            //Strech the wall along the x axsies in order to mach the requested wall lenght		
            wallToPostionGameObject.transform.localScale = new Vector3(MazeInfo.Instance.WallThickness / 2 - 0.01f / 2 + squareSize, wallToPostionGameObject.transform.localScale.y, wallGameObj.transform.localScale.z);
            wallToPostionGameObject.transform.parent = parent;
            //Resize the wall along the Y axes to match the selected WallThickness, it must be done after have setted the gameobject as parent 
            wallToPostionGameObject.transform.localScale = new Vector3(wallToPostionGameObject.transform.localScale.x, MazeInfo.Instance.WallThickness, wallGameObj.transform.localScale.z);
            //Rotate the wall if is his orientation is vertical
            if (wall.wallDirection == Directions.West || wall.wallDirection == Directions.East)
            {
                wallToPostionGameObject.transform.Rotate(new Vector3(0, 0, 90));
            }
            return wallToPostionGameObject;
        }

#if UNITY_EDITOR
        //Load the wall From the prefab in the editor
        public static GameObject GetWall(float wallThickness)
        {
            GameObject wall = (GameObject)UnityEditor.EditorGUIUtility.Load(Constants.PefabNames.WALL_2D + ".prefab");
            wall.transform.localScale = new Vector3(wall.transform.lossyScale.x, wallThickness, 0);
            Assert.IsTrue(wall != null, "ERROR: The Wall prefab has not been found");
            return wall;
        }
#endif
    }
}