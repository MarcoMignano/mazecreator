﻿using UnityEngine;
using System.Collections;
using UnityEngine.Assertions;
using System.Collections.Generic;

namespace MazeCreator
{
public static class MazeBuilder 
{
	public static Cell[,] InitializeMaze(int mazeRaws, int mazeCoulums)
	{
		Cell[,] maze = new Cell[mazeRaws, mazeCoulums];
		for (int i = 0; i < maze.GetLength(0); i++)
		{
			for (int j = 0; j < maze.GetLength(1); j++)
			{
				maze[i,j] = new Cell(i,j);
			}	
		}
		return maze;
	}	
	public static void BuildMaze(Cell[,] maze, float mazeMargin, GameObject wallGameObj,float wallSize)
	{
		MazeInfo.Instance.InitMazeInfo(maze.GetLength(0),mazeMargin,wallSize);
		MazeInfo mazeInfo = MazeInfo.Instance;		 		 		
		DrawMaze(maze,mazeInfo,wallGameObj);		
	}

	//Return the position of the wall inside the cell
	public static Vector2 GetWallPosition(Directions wallDirection, float squareSize, Vector2 cellCenter)
	{
		Vector2 wallPosition = cellCenter;
		switch (wallDirection)
			{
				case Directions.West:
					wallPosition.x -= squareSize/2;
					break;
				case Directions.North:			
					wallPosition.y += squareSize/2;
					break;			
				case Directions.East:
					wallPosition.x += squareSize/2;
					break;
				case Directions.South:
					wallPosition.y -= squareSize/2;
					break;
			}		
		return wallPosition;
	}
	//Create and Draw the maze into the scene
	private static void DrawMaze(Cell[,] maze, MazeInfo mazeInfo,GameObject wallGameObj)
	{
		GameObject mazeGameObject = new GameObject(Constants.GameObjecName.MAZE);			
		/*Vectore used to rappresent the position of each cell, not create in the for loop to avoid
		unnecessary memory allocation
		*/		
		Vector2 cellCenter = Vector2.zero;
		for (int raw = 0; raw < maze.GetLength(0); raw++)
		{
			for (int coulums = 0; coulums < maze.GetLength(1); coulums++)
			{
				cellCenter.y = mazeInfo.MazeOrigin.y - raw * mazeInfo.SquareSize;
				cellCenter.x = mazeInfo.MazeOrigin.x + coulums * mazeInfo.SquareSize;
				//Create and draw the actuall cell and set each cell as a child of the maze game object
				DrawCell(cellCenter,mazeInfo.SquareSize,maze[raw,coulums],wallGameObj).transform.parent = mazeGameObject.transform;
			}	
		}
		//Add the component to perform the Pathfinding search
		mazeGameObject.AddComponent<PathFindingGridManager>();
	}
	//Used to create a cell and position it 
	private static GameObject CreateCell(Vector2 cellCenter, float squareSize, Cell cell, GameObject wallPrefab)
	{
		GameObject cellGameObj = new GameObject(MazeUtilities.GetCellNameFromCell(cell));			
		cellGameObj.transform.position = cellCenter;		
		cellGameObj.transform.localScale = new Vector3(squareSize,squareSize,0);
		AddComponentsToCell(cellGameObj,cell.CellIndexInGrid,wallPrefab);
		return cellGameObj;
	}


	//Draw the celle and each wall witin each cell
	private static GameObject DrawCell(Vector2 cellCenter, float squareSize, Cell cell, GameObject wallGameObj)
	{
		//Find the position of each wall to Draw
		Vector2 wallPosition = Vector2.zero;
		GameObject cellGameObj = CreateCell(cellCenter,squareSize,cell,wallGameObj);		
		foreach (Wall wall in cell.Walls)
		{
			wallPosition = GetWallPosition(wall.wallDirection,squareSize,cellCenter);		
			cellGameObj.GetComponent<CellManager>().CreateWall(wall.wallDirection);
		}
		return cellGameObj;	
	}

	private static void AddComponentsToCell(GameObject cell, CellIndex index, GameObject wallPrefab)
	{
		cell.AddComponent<CellOverlayBackground>();
		cell.AddComponent<CellManager>().InitCellManager(index,wallPrefab);
	}
}
}