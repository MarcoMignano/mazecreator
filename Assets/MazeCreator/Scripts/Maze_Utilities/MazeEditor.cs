﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MazeCreator
{
//Class used in the editor to update the UP properties of the maze
public static class MazeEditor {

	public static void ResizeWall(float newWallSize)
	{
		foreach (WallManager wall in GameObject.FindObjectsOfType<WallManager>())
		{	
			wall.transform.localScale = new Vector3(wall.transform.localScale.x + newWallSize - wall.transform.localScale.y,newWallSize,0);
		}
	}
}
}