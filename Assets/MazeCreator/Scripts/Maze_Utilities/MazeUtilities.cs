﻿using System;
using UnityEngine;
using System.Linq;
using System.Collections;
using UnityEngine.Assertions;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace MazeCreator
{
//Helpes to interact with the maze
public static class MazeUtilities 
{
	public static string GetCellNameFromIndex(CellIndex cellIndex)
	{
		return "cell_"+ cellIndex.i + "-" + cellIndex.j;
	}

	public static string GetCellNameFromCell(Cell cell)
	{
		return "cell_"+ cell.CellIndexInGrid.i + "-" + cell.CellIndexInGrid.j;
	}

	//Return the cell Index that rapresent the index of the cell in the matrix
	public static CellIndex GetCellIndexFromName(string cellName)
	{
		int[] index = new Regex(@"\d+").Matches(cellName)
                              .Cast<Match>()
                              .Select(m => Int32.Parse(m.Value))
                              .ToArray();
							  
		Assert.IsTrue(index.Length == 2,"ERROR: No index found in cell " + cellName);
		return new CellIndex(index[0],index[1]);
	}

	public static CellIndex GetNeighbordIndexFromDirection(CellIndex currentIndex, Directions direction)
	{
		switch (direction)
		{
			case Directions.North:
			{
				return new CellIndex(currentIndex.i - 1,currentIndex.j + 0);								
			}			
			case Directions.East:
			{
				return new CellIndex(currentIndex.i + 0,currentIndex.j + 1);				
			}
			case Directions.South:
			{
				return new CellIndex(currentIndex.i + 1,currentIndex.j + 0);								
			}
			case Directions.West:
			{
				return new CellIndex(currentIndex.i + 0,currentIndex.j - 1);								
			}	
			default:
			{
				return new CellIndex(currentIndex.i - 1,currentIndex.j + 0);
			}			
		}
	}

	public static Wall[] GetWallsFromCell(GameObject cell)
	{	
		List <Wall> walls = new List<Wall>(cell.transform.childCount);
		foreach (GameObject gameObjWall in cell.transform.GetChilds())
		{
			if (gameObjWall.name.Contains(Constants.GameObjecName.WALL_ID))
			{
				Directions wallDirection = GetDirectionFromWallName(gameObjWall.name);
				walls.Add(new Wall(wallDirection));
			}
		}
		return walls.ToArray();
	}

	public static bool IsInsideMaze(CellIndex cellIndex)
	{
		return ((cellIndex.i <= MazeInfo.Instance.MazeSizeY && cellIndex.i >= 0) && (cellIndex.j <= MazeInfo.Instance.MazeSizeX && cellIndex.j >= 0));
	}
	public static Directions GetDirectionFromWallName(string wallName)
	{
		Directions wallDirection = Directions.Undefined;
		foreach (Directions direction in Enum.GetValues(typeof(Directions)))
		{
			if (wallName.Contains(direction.ToString()))
			{
				wallDirection = direction; 
			}
		}				
		return wallDirection;		
	}
}
}