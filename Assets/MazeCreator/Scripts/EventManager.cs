﻿using UnityEngine;
using System.Collections;

namespace MazeCreator
{
public class EventManager : MonoBehaviour {

    #region PROPERTIES
    //Enum EvenType per identificare il tipo di evento da scatenare
    public enum EventType { CleanMaze }

    #endregion

    #region Delegate
    public delegate void CleanMaze();
    #endregion

    #region Events
    public static event CleanMaze onCleanMazeDelegate;
    #endregion

    #region METHODS
    public static void LaunchEvent(EventType type)
    {
        switch (type)
        {
            case EventType.CleanMaze:
                OnCleanMazeEvent();
                break;
            default:
                break;
        }
    }
    #endregion

    #region EVENTS LAUNCHER

    private static void OnCleanMazeEvent()
    {
        if (onCleanMazeDelegate != null)
        {
            onCleanMazeDelegate();
        }
    }
    #endregion
}
}