﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using UnityEngine.Assertions;
using System.Collections.Generic;
using MazeCreator;

public class MazeBuilderWindow : EditorWindow {

#region Fields	
	public Transform pathBeguin;
	public Transform pathEnd;
	private int _mazeRaws = 10;	
	private CellOverlayBackground lastSelectedCell = null;
	private Cell[,] _maze;
	private Node [] _pathToShow;
	private bool _mazeBuilded = false;
	private float _mazeMargin = 0.1f;
	private float _wallThickness = 0.06f;
#endregion
	
#region Unity Lifecycle

	void OnEnable()
	{
		SceneView.onSceneGUIDelegate += this.OnSceneGUI;
	}
	
	void OnGUI()
	{
		
		CreateNewMazeWindowView();
		CreateNewWallWindowView();
		RemoveWallWindowView();	
		CleanMazelWindowView();		
		DeleteMazeWindowsView();
		LoadMazeWindowView();
		FindPathWindowsView();	
	 }

	void Ondisable()
	{
	 	SceneView.onSceneGUIDelegate -= this.OnSceneGUI;		 
	}
#endregion

#region Draw Object
	//Secion used to draw the objects on the scene
	void OnSceneGUI( SceneView sceneView )
    {		
		if (_mazeBuilded)
		{			 
			DrawCellCenterPoints(this._maze,_mazeMargin);
			DrawCellOverlay();		
			DrawPath();	
		}
    }

	//Used to draw the cell ovelay when the mouse goes over the cell
	private void DrawCellOverlay()
	{
		Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);			
	    RaycastHit2D hit = Physics2D.Raycast (ray.origin, ray.direction, Mathf.Infinity);
		if (hit.collider != null)
		{				
			CellOverlayBackground newCell = hit.collider.gameObject.GetComponent<CellOverlayBackground>();
			if (lastSelectedCell && newCell.transform.name != lastSelectedCell.transform.name)
			{
				lastSelectedCell.OnMouseExit();
			}
			if (lastSelectedCell != null)
			{
				lastSelectedCell = newCell;
				lastSelectedCell.OnMouseEnter();
			}
		}
		else if (lastSelectedCell != null)
		{
			lastSelectedCell.OnMouseExit();
		}
	}

	//Used to show the center of each cell
	public static void DrawCellCenterPoints( Cell[,] maze, float mazeMargin)
	{
		if (maze != null)
		{
			MazeInfo mazeInfo = MazeInfo.Instance;			 
		 	/*Vector used to rappresent the position of each cell, not create in the for loop to avoid
			unnecessary memory allocation
			*/
			Vector2 cellPosition = Vector2.zero;
			for (int raw = 0; raw < maze.GetLength(0); raw++)
			{
				for (int coulums = 0; coulums < maze.GetLength(1); coulums++)
				{
					cellPosition.y = mazeInfo.MazeOrigin.y - raw * mazeInfo.SquareSize;
					cellPosition.x = mazeInfo.MazeOrigin.x + coulums * mazeInfo.SquareSize;
					Handles.color = Color.red;
					Handles.DrawWireDisc(cellPosition,new Vector3(0,0,1),0.01f);					
				}	
			}
		}
	}

	private void DrawPath()
	{
		if(_pathToShow != null && _pathToShow.Length > 1)
		{
			for (int i = 1,j = 0; i < _pathToShow.Length; i++,j++)
			{
				Node startNode = _pathToShow[j];
				Node secondNode = _pathToShow[i];
				Handles.color = Color.red;
				Handles.DrawLine(startNode.position,secondNode.position);
			}
		}
	}
#endregion

#region Create maze
	private void Create2DMaze()
	{
		GameObject wall = WallBuilder.GetWall(_wallThickness);
        _maze = MazeBuilder.InitializeMaze(_mazeRaws,_mazeRaws);
		_maze = DFS.CreateMaze(_maze);
		MazeBuilder.BuildMaze(_maze,_mazeMargin,wall,_wallThickness);
		_mazeBuilded = true;		
	}
	private void CreateNewMazeWindowView()
	{
		GUILayout.Label ("Create Maze", EditorStyles.boldLabel);	
		EditorGUILayout.BeginHorizontal();	  
		if(GUILayout.Button("2D Maze"))
        {
			Create2DMaze();
        }
		if(GUILayout.Button("Delete And Create"))
        {
            DeleteMazes();
			Create2DMaze();
        }		 		
		EditorGUILayout.EndHorizontal();
		EditorGUILayout.Space();	
		_mazeMargin = EditorGUILayout.Slider("Margin from screen %",_mazeMargin,0f,0.3f);		
		float newWallThickness = EditorGUILayout.Slider("Wall thikness",_wallThickness,0f,0.4f);
		if (newWallThickness != _wallThickness)
		{
			_wallThickness = newWallThickness;
			MazeEditor.ResizeWall(newWallThickness);
		}
		EditorGUILayout.Space();
		_mazeRaws =EditorGUILayout.IntField("Size of the maze:", _mazeRaws);	
	}

#endregion

#region Delete maze
    //Section used to delete the maze from the scene
	private void DeleteMazeWindowsView()
	{
		if (_maze != null)
		{
			GUILayout.Label ("Delete Maze", EditorStyles.boldLabel);	
			EditorGUILayout.BeginHorizontal();	  
			if(GUILayout.Button("Delete Maze"))
        	{
				DeleteMazes();
        	} 		
			EditorGUILayout.EndHorizontal();	
		}
	}		
	private void DeleteMazes()
	{	
		GameObject maze = GameObject.Find(Constants.GameObjecName.MAZE);
		while (maze != null)
		{
			DestroyImmediate(maze);
			maze = null;		
			maze = GameObject.Find(Constants.GameObjecName.MAZE);
		}
		
	}
#endregion

#region PathFinding
	private void FindPathWindowsView()
	{
		EditorGUILayout.Space();
		EditorGUILayout.BeginVertical();	  
		GUILayout.Label ("PathFinding", EditorStyles.boldLabel);		
		pathBeguin = (Transform) EditorGUILayout.ObjectField("Start",pathBeguin,typeof(Transform),true);
		pathEnd = (Transform) EditorGUILayout.ObjectField("End",pathEnd,typeof(Transform),true);
		EditorGUILayout.EndVertical();
		if(GUILayout.Button("Find Path"))
        {
			_pathToShow = AStar.FindPath(pathBeguin.position, pathEnd.position).ToArray();			
			EditorUtility.SetDirty(pathEnd);			
        }
	}
#endregion

#region Clean Maze
	//Used to remove from the maze the scripts used in editor mode
	void CleanMazelWindowView()
	{
		EditorGUILayout.Space();
		GUILayout.Label ("Claen Maze", EditorStyles.boldLabel);	
		EditorGUILayout.BeginVertical();	  
		if(GUILayout.Button("Clean Maze"))
        {
			CleanMaze();
        }				
		EditorGUILayout.HelpBox("Remember to clean the maze before saving it as a prefab", MessageType.Info);
		EditorGUILayout.EndVertical();		
		EditorGUILayout.Space();
	}

	private void CleanMaze()
	{
		EventManager.LaunchEvent(EventManager.EventType.CleanMaze);
	}

#endregion

#region Create/remove wall
	private GameObject GetSelectedCell()
	{		
		if (Selection.gameObjects.Length > 0)
		{
			GameObject cell = Selection.gameObjects[0];
			if ( cell!= null && cell.GetComponent<CellManager>() != null)
			{
				return cell;
			}
			else return null;			
		}		
		else return null;
	}
	private void CreateNewWallWindowView()
	{
		EditorGUILayout.Space();
		GUILayout.Label ("Add Wall", EditorStyles.boldLabel);	
		EditorGUI.BeginDisabledGroup (GetSelectedCell() == null);
		EditorGUILayout.BeginHorizontal();	  
		if(GUILayout.Button("Top"))
        {						
			WallBuilder.CreateWall(GetSelectedCell().GetComponent<CellManager>(), Directions.North);
			FindObjectOfType<PathFindingGridManager>().BuilGrid();
        }
		if(GUILayout.Button("Right"))
        {
            WallBuilder.CreateWall(GetSelectedCell().GetComponent<CellManager>(), Directions.East);
			FindObjectOfType<PathFindingGridManager>().BuilGrid();
        }		 
		if(GUILayout.Button("Bottom"))
        {
            WallBuilder.CreateWall(GetSelectedCell().GetComponent<CellManager>(), Directions.South);
			FindObjectOfType<PathFindingGridManager>().BuilGrid();
        }		
		if(GUILayout.Button("Left"))
        {
            WallBuilder.CreateWall(GetSelectedCell().GetComponent<CellManager>(), Directions.West);
			FindObjectOfType<PathFindingGridManager>().BuilGrid();
        }
		EditorGUILayout.EndHorizontal();
		EditorGUI.EndDisabledGroup();
	}
	private void RemoveWallWindowView()
	{
		EditorGUILayout.Space();
		GUILayout.Label ("Remove Wall", EditorStyles.boldLabel);	
		EditorGUI.BeginDisabledGroup (GetSelectedCell() == null);
		EditorGUILayout.BeginHorizontal();	  
		if(GUILayout.Button("Top"))
        {						
			WallBuilder.RemoveWall(GetSelectedCell().GetComponent<CellManager>(), Directions.North);
			FindObjectOfType<PathFindingGridManager>().BuilGrid();
        }
		if(GUILayout.Button("Right"))
        {
            WallBuilder.RemoveWall(GetSelectedCell().GetComponent<CellManager>(), Directions.East);
			FindObjectOfType<PathFindingGridManager>().BuilGrid();
        }		 
		if(GUILayout.Button("Bottom"))
        {
            WallBuilder.RemoveWall(GetSelectedCell().GetComponent<CellManager>(), Directions.South);
			FindObjectOfType<PathFindingGridManager>().BuilGrid();
        }		
		if(GUILayout.Button("Left"))
        {
            WallBuilder.RemoveWall(GetSelectedCell().GetComponent<CellManager>(), Directions.West);
			FindObjectOfType<PathFindingGridManager>().BuilGrid();
        }
		EditorGUILayout.EndHorizontal();
		EditorGUI.EndDisabledGroup();
	}
#endregion

#region Load maze
	//Section used to initilize the data structures needed to edit the maze from the scene
	private void LoadMazeWindowView()
	{
		EditorGUILayout.Space();
		GUILayout.Label ("Load Maze", EditorStyles.boldLabel);
		if(GUILayout.Button("Load Maze"))
    	{
			 MazeLoader.LoadMaze(WallBuilder.GetWall(_wallThickness));
    	}
		_mazeBuilded = true;
	}
#endregion

	[MenuItem ("Window/Maze Builder")]
    public static void  InitWindow () 
	{
        MazeBuilderWindow window = (MazeBuilderWindow) EditorWindow.GetWindow(typeof(MazeBuilderWindow),false,"Maze Builder");
		window.Show();
    }
	
}
