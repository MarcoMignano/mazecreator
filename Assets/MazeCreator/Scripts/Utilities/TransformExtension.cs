﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TransformExtension  {

	public static List<GameObject> GetChilds(this Transform items)
	{
		List <GameObject> childs = new List<GameObject>();
    	foreach (Transform item in items.transform)
		{
        	childs.Add(item.gameObject);
		}
		return childs;
	}
}
