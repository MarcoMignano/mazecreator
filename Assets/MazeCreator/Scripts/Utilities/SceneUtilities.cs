﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SceneUtilities {
	private static Camera _camera;
	public static Camera Camera 
	{
		get 
		{
			if (_camera == null)
			{
				_camera = Camera.main;
			}
			return _camera;
		}
	} 
	//Returns all the cells from the maze
	public static List<GameObject> GetChilds(GameObject parent, string childName)
	{
		List<GameObject> cells = new List<GameObject>();
		foreach (Transform child in parent.transform)
		{
			if (child.name.Contains(childName))
			{
				cells.Add(child.gameObject);
			}			
		}
		return cells;
	}	
}
