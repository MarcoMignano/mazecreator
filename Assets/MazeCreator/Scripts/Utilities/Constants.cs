﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Constants 
{
	public static class PefabNames
	{
		public static string WALL_2D = "Wall2D";	
	}

	public static class Resurces
	{
		public static string CELL_BACKGROUND = "Cell_Background";	
	}

	public static class GameObjecName
	{
		public static string MAZE = "Maze";	
		public static readonly string WALL_ID = "wall";
	}	
	
}
