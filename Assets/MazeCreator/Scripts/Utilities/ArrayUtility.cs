﻿using System;
using System.Linq;
using System.Collections.Generic;

public static class ArrayUtility {

	private static Random rnd = new Random();
	public static void Shuffle<T>(this T[] array)
	{
		int n = array.Length;
		while (n > 1)
		{
			n--;
			int k = rnd.Next(n + 1);
			T value = (T) array.GetValue(k);
			//Exange the values
			array.SetValue(array.GetValue(n),k);
			array.SetValue(value,n);
		}
	}	
	public static void Each<T>(this IEnumerable<T> items, Action<T> action)
	{
    	foreach (var item in items)
		{
        	action(item);
		}
	}
}
